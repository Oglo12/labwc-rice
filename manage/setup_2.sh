#!/usr/bin/env bash

source ~/.config/labwc/lib.sh

if [[ -f $HOME/.bashrc ]]; then
    echo "source ~/.config/labwc/aliases/bash.sh" >> $HOME/.bashrc
fi
