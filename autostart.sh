#!/usr/bin/env bash

source ~/.config/labwc/lib.sh

run_hook pre

brightnessctl --restore

swww init
set_wallpaper ~/.config/labwc/wallpaper.png

~/.config/labwc/waybar/start
~/.config/labwc/swaync/start
~/.config/labwc/eww/start

nm-applet &
blueman-applet &

lxsession &

run_hook post
